#include <iostream>

int substr(std::string, std::string);
void handle_error(int);

int error = 0;
std::string errors[5] = {
    "Not enough arguments", "Too many arguments", "String too short (min. 2)",
    "Pattern too short (min. 2 max. 2)", "Pattern too long (min. 2 max. 2)"};

int main(int argc, char *argv[]) {
  if (argc < 3) {
    handle_error(1);
  } else if (argc > 3) {
    handle_error(2);
  }

  std::string a = argv[1];
  if (a.length() < 2) {
    handle_error(3);
  }

  std::string pattern = argv[2];
  if (pattern.length() < 2) {
    handle_error(4);
  } else if (pattern.length() > 2) {
    handle_error(5);
  }

  printf("There are %d '%s' in '%s'\n", substr(a, pattern), pattern.c_str(),
         a.c_str());

  return 0;
}

int substr(
    std::string s,
    std::string sub) { // Search for pattern of two characters inside a string
  int len = s.length();
  int a = 0;
  std::string tmp = "";
  for (int i = 0; i < len; i++) {
    tmp = s[i];
    tmp += s[i + 1];
    if (tmp == sub) {
      ++a;
    }
  }

  return a;
}

void handle_error(int e) {
  fprintf(stderr, "Error %d: %s\n", e, errors[e - 1].c_str());
  exit(e);
}
