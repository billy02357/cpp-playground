#include <iostream>

struct Date {
  int day;
  int month;
  int year;
};

char months[12][12] = {"January",   "February", "March",    "April",
                       "May",       "June",     "July",     "August",
                       "September", "October",  "November", "December"};

Date read_date_stdin(void);
void print_date(Date);

int main(void) {
  Date date;
  date = read_date_stdin();
  print_date(date);
  return 0;
}

Date read_date_stdin(void) {
  Date date;
  std::string in;
  std::cin >> in;

  int mstart = 0;
  if (in[1] == '/') {
    mstart = 2; // Found slash in the second character: month number starts at
                // third (index 2).
    const char *dayc = &in[0]; // Get the first character (day).
    date.day =
        atoi(dayc); // Convert it to integer and store it on the date struct.
  } else if (in[2] == '/') {
    mstart = 3; // If the third character is a slash, month number starts at
                // forth (index 3).
    char cday[3];
    cday[0] = in[0];
    cday[1] = in[1];
    cday[2] = '\0'; // Get a char array from first two characters and convert to
                    // integer, store it on struct.
    date.day = atoi(cday);
  }

  int ystart = 0;
  if (in[mstart + 1] ==
      '/') { // If a slash is found one char after where month number starts, it
             // means the year number starts two chars after.
    ystart = mstart + 2;
    const char *monthc = &in[mstart]; // Get the month char
    date.month = atoi(monthc);        // Convert to int
  } else if (in[mstart + 2] == '/') {
    ystart = mstart + 3; // If the slash is found after two chars, the year
                         // starts three chars after the month.
    char cmonth[3];
    cmonth[0] = in[mstart + 1];
    cmonth[1] = in[mstart + 2];
    cmonth[2] = '\0'; // Get a char array from the two month numbers convert to
                      // int and store in struct.
    date.month = atoi(cmonth);
  }

  char yearc[200];
  int l;
  for (int i = 0; in[ystart + i] != '\0';
       i++) { // Get the rest of the chars inside yearc and convert to int and
              // store in struct.
    yearc[i] = in[ystart + i];
    l = i + 1;
  }
  yearc[l] = '\0';
  date.year = atoi(yearc);

  return date;
}

void print_date(Date date) { // Print the date in a long format ex.: "Today is
                             // August the 3rd of 2008".

  printf("Today is %s ", months[date.month - 1]);

  switch (date.day) {
  case 1:
    printf("the 1st");
    break;

  case 2:
    printf("the 2nd");
    break;

  case 3:
    printf("the 3rd");
    break;

  default:
    printf("the %dth", date.day);
    break;
  }

  printf(" of %d\n", date.year);
}
