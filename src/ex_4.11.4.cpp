// Print letters and digits and their integer values
#include <cstdio>
#include <iostream>

int main(void) {
  std::string alphabet =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  for (int i = 0; i < alphabet.length(); i++) {
    std::cout << alphabet[i] << ": " << int(alphabet[i]);
    printf("    0x%X    0%o\n", int(alphabet[i]), int(alphabet[i]));
  }

  return 0;
}
