#include <cctype>
#include <cstring>
#include <iostream>
#include <vector>

void hex_string_to_char(std::vector<char>);
int convert_hex_to_dec(char[]);
void print_help(void);
void error(const char *);

int main(int argc, char *argv[]) {
  char ch;
  std::vector<char> chv;

  if (argc == 1) {
    ch = getchar();
    while (ch != EOF) {
      chv.push_back(ch);
      ch = getchar();
    }
    hex_string_to_char(chv);

  } else if (argc > 1) {
    if (!strcmp(argv[1], "-h")) {
      print_help();
    }
    std::string args;
    for (int i = 1; i < argc; i++) {
      args += argv[i];
      args += " ";
    }
    for (int i = 0; i < args.length(); i++) {
      chv.push_back(args[i]);
    }
    hex_string_to_char(chv);
  }

  std::cout << std::endl;
  return 0;
}

void hex_string_to_char(std::vector<char> v) {
  char charray[v.size() + 1];
  for (int i = 0; i < v.size(); i++) {
    charray[i] = v[i];
  }

  char ch[2];
  for (int i = 0; i < strlen(charray); i += 3) {
    ch[0] = charray[i];
    ch[1] = charray[i + 1];
    printf("%c", convert_hex_to_dec(ch));
  }
}

int convert_hex_to_dec(char num[]) {
  int base = 1;
  int temp = 0;
  for (int i = strlen(num) - 1; i >= 0; i--) {
    if (num[i] >= '0' && num[i] <= '9') {
      temp += (num[i] - 48) * base;
      base = base * 16;
    } else if (num[i] >= 'A' && num[i] <= 'F') {
      temp += (num[i] - 55) * base;
      base = base * 16;
    }
  }

  return temp;
}

void error(const char *s) {
  std::cerr << s << std::endl;
  exit(EXIT_FAILURE);
}

void print_help(void) {
  error("Usage: hex_to_ascii [-h] [HEX][HEX]...\n\n"
        "Be sure to enter in a format of\none ASCII code per argument: "
        "hex_to_ascii 48 65 6C 6C 6F 20");
}
