#include <iostream>
#include <string>
#include <vector>

void print_entry(int);
void add_entries(int);

struct Entry {
  std::string name;
  int number;
};

std::vector<Entry> phone_book(1);

int main(void) {

  phone_book[0].name = "John";
  phone_book[0].number = 893645769;

  for (int i = 0; i < phone_book.size(); i++) {
    print_entry(i);
  }

  return 0;
}

void print_entry(int i) {
  std::cout << phone_book[i].name << "\t" << phone_book[i].number << "\n";
}

void add_entries(int n) { phone_book.resize(phone_book.size() + n); }
