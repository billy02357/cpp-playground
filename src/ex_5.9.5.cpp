// Exercise 5.9.5: sizeof() counts the NULL char, strlen does not.
#include <cstring>
#include <iostream>

int main(void) {
  char str[] = "a short string";
  std::cout << "sizeof: " << sizeof(str)
            << "\n"
               "length: "
            << strlen(str) << "\n";

  return 0;
}
