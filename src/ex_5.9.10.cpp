#include <iostream>

void print_string_array(std::string[], std::string, int);

const int months_in_a_year = 12;

int main(void) {

  std::string months[months_in_a_year] = {
      "January", "February", "March",     "April",   "May",      "June",
      "July",    "August",   "September", "October", "November", "December"};
  print_string_array(months, "\n", months_in_a_year);
  print_string_array(months, ", ", months_in_a_year);
  return 0;
}

void print_string_array(std::string s[], std::string sep, int len) {
  for (int i = 0; i < len; i++) {
    std::cout << s[i] << sep;
  }
  if (sep != "\n") {
    std::cout << '\n';
  }
}
