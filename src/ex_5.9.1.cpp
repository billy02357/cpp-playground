// Exercise 1 from chapter 5 section 9
#include <iostream>

int main(void) {
  char a = 'a';
  char *ap = &a; // Pointer to char

  int inta[10];            // Array of ten integers
  int(&intref)[10] = inta; // Reference to an array of ten integers

  char s[3] = {'a', 'b', 'c'};
  char *sp = s; // Pointer to an array of characters

  char b = 'b';
  char *bp = &b;
  char **bpp = &bp; // Pointer to a pointer to a character

  const int consti = 120;     // Constant integer
  const int constip = consti; // Pointer to a constant integer

  int i = 100;
  int *const cpi = &i; // Constant pointer to an integer

  return 0;
}
