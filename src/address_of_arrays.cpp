#include <iostream>

int main(void) {

  int vi[10];
  short vs[10];

  printf("0x%X 0x%X\n", &vi[0], &vi[1]);
  printf("0x%X 0x%X\n", &vs[0], &vs[1]);

  return 0;
}
