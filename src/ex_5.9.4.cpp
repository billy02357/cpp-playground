// Exercise 5.9.4: Swap two int with pointers and references
#include <iostream>

void int_pointer_swap(int *, int *);
void int_reference_swap(int &, int &);

int main(void) {
  int i = 100;
  int d = 200;
  std::cout << "Before int_pointer_swap():\n"
               "i: "
            << i
            << "\n"
               "d: "
            << d << "\n\n";

  int_pointer_swap(&i, &d);

  std::cout << "After int_pointer_swap():\n"
               "i: "
            << i
            << "\n"
               "d: "
            << d << "\n\n";

  std::cout << "Before int_reference_swap():\n"
               "i: "
            << i
            << "\n"
               "d: "
            << d << "\n\n";
  int &g = i;
  int &h = d;
  int_reference_swap(g, h);

  std::cout << "After int_reference_swap():\n"
               "i: "
            << i
            << "\n"
               "d: "
            << d << "\n";

  return 0;
}

void int_pointer_swap(int *a, int *b) {
  int atmp = *a;
  *a = *b;
  *b = atmp;
}

void int_reference_swap(int &a, int &b) {
  int atmp = a;
  a = b;
  b = atmp;
}
