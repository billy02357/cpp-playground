#include <algorithm>
#include <iostream>
#include <vector>

void print_vector_items(std::vector<std::string>, std::string);
bool str_is_inside_vector(std::string, std::vector<std::string>);

int main(void) {
  std::string word = "";

  std::vector<std::string> words;

  while (word != "quit") { // Keep getting words until it founds "quit"
    std::cout << "> "; // FIXME: Bug: after input of more than one word, this
                       // loop will "compensate" by iterating as much times as
                       // words inputed, so the prompt is printed many times.

    std::cin >> word;
    // Store the word inside the vector only if it is unique, not an empty
    // string or "quit"
    if (word != "quit" && word != "" && !str_is_inside_vector(word, words)) {
      words.push_back(word);
    }
  }

  // Sort vector items in alphabetical order
  std::sort(words.begin(), words.end());

  std::cout << "\nWords: ";
  print_vector_items(words, ", "); // Print words separated by ,

  return 0;
}

void print_vector_items(
    std::vector<std::string> v,
    std::string sep) { // Prints every item in v, separated by sep
  int len = v.size();
  for (int i = 0; i < len; i++) {
    if (i != len - 1) {
      std::cout << v[i] << sep;
    } else {
      std::cout << v[i];
    }
  }
  if (sep != "\n") {
    std::cout << std::endl;
  }
}

bool str_is_inside_vector(
    std::string s,
    std::vector<std::string> v) { // Returns true if s is found inside v
  int len = v.size();
  for (int i = 0; i < len; i++) {
    if (v[i] == s)
      return true;
  }
  return false;
}
