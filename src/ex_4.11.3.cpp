// Print sizes of fundamental types
#include <iostream>

int main(void) {
  struct E {};
  using namespace std;
  cout << sizeof(unsigned char) << "\n";
  cout << sizeof(signed char) << "\n";
  cout << sizeof(char) << "\n";
  cout << sizeof(short int) << "\n";
  cout << sizeof(int) << "\n";
  cout << sizeof(long int) << "\n";
  cout << sizeof(float) << "\n";
  cout << sizeof(double) << "\n";
  cout << sizeof(long double) << "\n";
  cout << sizeof(E) << "\n";
  cout << sizeof(string) << "\n";
  long double i =
      999999999999999999999999999999999999999999999999999999999999999999999999999994253627926889346572465321569349999999999999999999123412414.23419786349876234897619851934875892374598237458293457234852357896179416298743683912746;
  int a = 1;
  cout << i + a << ":\t" << sizeof(i + a) << "\n";
  cout << sizeof("ñ") << "\n";

  return 0;
}
