// Print month names and days with arrays and structs
#include <iostream>

const int months_in_a_year = 12;
const int string_max = 200;

int main(void) {
  char month_names[months_in_a_year][string_max] = {
      "January", "February", "March",     "April",   "May",      "June",
      "July",    "August",   "September", "October", "November", "December"};

  int month_days[months_in_a_year] = {31, 28, 31, 30, 31, 30,
                                      31, 31, 30, 31, 30, 31};

  for (int i = 0; i < months_in_a_year; i++) {
    std::cout << month_names[i] << ": " << month_days[i] << std::endl;
  }

  struct Month {
    char name[string_max];
    int days;
  };

  Month months[months_in_a_year];

  months[0] = {"January", 31};
  months[1] = {"February", 28};
  months[2] = {"March", 31};
  months[3] = {"April", 30};
  months[4] = {"May", 31};
  months[5] = {"June", 30};
  months[6] = {"July", 31};
  months[7] = {"August", 31};
  months[8] = {"September", 30};
  months[9] = {"October", 31};
  months[10] = {"November", 30};
  months[11] = {"December", 31};

  for (int i = 0; i < months_in_a_year; i++) {
    std::cout << months[i].name << ": " << months[i].days << std::endl;
  }

  return 0;
}
