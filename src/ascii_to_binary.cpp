#include <cstring>
#include <iostream>
#include <vector>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)                                                   \
  (byte & 0x80 ? '1' : '0'), (byte & 0x40 ? '1' : '0'),                        \
      (byte & 0x20 ? '1' : '0'), (byte & 0x10 ? '1' : '0'),                    \
      (byte & 0x08 ? '1' : '0'), (byte & 0x04 ? '1' : '0'),                    \
      (byte & 0x02 ? '1' : '0'), (byte & 0x01 ? '1' : '0')

void print_int_vector_as_bin(std::vector<int>);
void print_help(void);
void error(const char *);

int main(int argc, char *argv[]) {
  int ch;
  std::vector<int> chv;

  if (argc == 1) {
    ch = getchar();
    while (ch != EOF) {
      chv.push_back(ch);
      ch = getchar();
    }
    print_int_vector_as_bin(chv);

  } else if (argc > 1) {
    if (!strcmp(argv[1], "-h")) {
      print_help();
    }
    std::string args;
    for (int i = 1; i < argc; i++) {
      args += argv[i];
      args += " ";
    }
    for (int i = 0; i < args.length(); i++) {
      printf(BYTE_TO_BINARY_PATTERN " ", BYTE_TO_BINARY(args[i]));
    }
  }

  printf("\n");

  return 0;
}

void print_int_vector_as_bin(std::vector<int> v) {
  for (int i = 0; i < v.size(); i++) {
    if (v[i] != '\0') {
      if (v[i] == '\n') {
        int _0a = 0x0A;
        printf(BYTE_TO_BINARY_PATTERN " ", BYTE_TO_BINARY(_0a));
      } else {
        printf(BYTE_TO_BINARY_PATTERN " ", BYTE_TO_BINARY(v[i]));
      }
    }
  }
}

void error(const char *s) {
  std::cerr << s << std::endl;
  exit(EXIT_FAILURE);
}

void print_help(void) { error("Usage: ascii_to_binary [-h] [TEXT]"); }
