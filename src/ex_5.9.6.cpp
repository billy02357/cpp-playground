// Testing legal calls and convertions
#include <iostream>

void f(char);
void g(char &);
void h(const char &);

int main(void) {
  char c = 'c';
  unsigned char uc = 'u';
  signed char sc = 's';
  f('a');  // ok: 'a' is a char
  f(49);   // ok: 49 is a valid ASCII number
  f(3300); // overflow!
  f(c);    // ok: 'c' is a char
  f(uc);   // ok: 'u' is a char
  f(sc);   // ok: 's' is a char

  g('a');  // error: cant convert to char&
  g(49);   // error: cant convert to char&
  g(3300); // overflow! and cant convert to char&
  g(c);    // ok: compiler creates a reference to 'c'
  g(uc);   // error: cant convert to char&
  g(sc);   // error: cant convert to char&

  h('a');  // ok: 'a' is copied to a temporary variable by the compiler in form
           // of const char&
  h(49);   // ok: 49 is valid ASCII -> copied to a temporary variable by the
           // compiler in form of const char&
  h(3300); // overflow!
  h(c);  // ok: 'c' is copied to a temporary variable by the compiler in form of
         // const char&
  h(uc); // ok: 'u' is copied to a temporary variable by the compiler in form of
         // const char&
  h(sc); // ok: 's' is copied to a temporary variable by the compiler in form of
         // const char&

  return 0;
}
