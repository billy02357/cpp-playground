#include <cstring>
#include <iostream>
#include <vector>

void print_int_vector_as_hex(std::vector<int>);
void print_help(void);
void error(const char *);

int main(int argc, char *argv[]) {
  int ch;
  std::vector<int> chv;

  if (argc == 1) {
    ch = getchar();
    while (ch != EOF) {
      chv.push_back(ch);
      ch = getchar();
    }
    print_int_vector_as_hex(chv);

  } else if (argc > 1) {
    if (!strcmp(argv[1], "-h")) {
      print_help();
    }
    std::string args;
    for (int i = 1; i < argc; i++) {
      args += argv[i];
      args += " ";
    }
    for (int i = 0; i < args.length(); i++) {
      printf("%X ", args[i]);
    }
  }

  printf("\n");
  return 0;
}

void print_int_vector_as_hex(std::vector<int> v) {
  for (int i = 0; i < v.size(); i++) {
    if (v[i] != '\0') {
      if (v[i] == '\n') {
        printf("0A ");
      } else {
        printf("%X ", v[i]);
      }
    }
  }
}

void error(const char *s) {
  std::cerr << s << std::endl;
  exit(EXIT_FAILURE);
}

void print_help(void) { error("Usage: ascii_to_hex [-h] [TEXT]"); }
